const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'mirador-bundle.min.js',
    path: path.resolve(__dirname, 'dist'),
    
  },
  mode: 'production',
  // devtool: "sourcemap",
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
  ],
  
  
};
